#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

/*
 * Write a WAVE file header to given output stream.
 * Format is hard-coded as 44.1 KHz sample rate, 16 bit
 * signed samples, two channels.
 *
 * Parameters:
 *   out - the output stream
 *   num_samples - the number of (stereo) samples that will follow
 */
void write_wave_header(FILE *out, unsigned num_samples) {
  /*
   * See: http://soundfile.sapp.org/doc/WaveFormat/
   */

  uint32_t ChunkSize, Subchunk1Size, Subchunk2Size;
  uint16_t NumChannels = NUM_CHANNELS;
  uint32_t ByteRate = SAMPLES_PER_SECOND * NumChannels * (BITS_PER_SAMPLE/8u);
  uint16_t BlockAlign = NumChannels * (BITS_PER_SAMPLE/8u);

  /* Subchunk2Size is the total amount of sample data */
  Subchunk2Size = num_samples * NumChannels * (BITS_PER_SAMPLE/8u);
  Subchunk1Size = 16u;
  ChunkSize = 4u + (8u + Subchunk1Size) + (8u + Subchunk2Size);

  /* Write the RIFF chunk descriptor */
  write_bytes(out, "RIFF", 4u);
  write_u32(out, ChunkSize);
  write_bytes(out, "WAVE", 4u);

  /* Write the "fmt " sub-chunk */
  write_bytes(out, "fmt ", 4u);       /* Subchunk1ID */
  write_u32(out, Subchunk1Size);
  write_u16(out, 1u);                 /* PCM format */
  write_u16(out, NumChannels);
  write_u32(out, SAMPLES_PER_SECOND); /* SampleRate */
  write_u32(out, ByteRate);
  write_u16(out, BlockAlign);
  write_u16(out, BITS_PER_SAMPLE);

  /* Write the beginning of the "data" sub-chunk, but not the actual data */
  write_bytes(out, "data", 4);        /* Subchunk2ID */
  write_u32(out, Subchunk2Size);
}

/*
 * Read a WAVE header from given input stream.
 * Calls fatal_error if data can't be read, if the data
 * doesn't follow the WAVE format, or if the audio
 * parameters of the input WAVE aren't 44.1 KHz, 16 bit
 * signed samples, and two channels.
 *
 * Parameters:
 *   in - the input stream
 *   num_samples - pointer to an unsigned variable where the
 *      number of (stereo) samples following the header
 *      should be stored
 */
void read_wave_header(FILE *in, unsigned *num_samples) {
  char label_buf[4];
  uint32_t ChunkSize, Subchunk1Size, SampleRate, ByteRate, Subchunk2Size;
  uint16_t AudioFormat, NumChannels, BlockAlign, BitsPerSample;

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "RIFF", 4u) != 0) {
    fatal_error("Bad wave header (no RIFF label)\n");
  }

  read_u32(in, &ChunkSize); /* ignore */

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "WAVE", 4u) != 0) {
    fatal_error("Bad wave header (no WAVE label)\n");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "fmt ", 4u) != 0) {
    fatal_error("Bad wave header (no 'fmt ' subchunk ID)\n");
  }

  read_u32(in, &Subchunk1Size);
  if (Subchunk1Size != 16u) {
    fatal_error("Bad wave header (Subchunk1Size was not 16)\n");
  }

  read_u16(in, &AudioFormat);
  if (AudioFormat != 1u) {
    fatal_error("Bad wave header (AudioFormat is not PCM)\n");
  }

  read_u16(in, &NumChannels);
  if (NumChannels != NUM_CHANNELS) {
    fatal_error("Bad wave header (NumChannels is not 2)\n");
  }

  read_u32(in, &SampleRate);
  if (SampleRate != SAMPLES_PER_SECOND) {
    fatal_error("Bad wave header (Unexpected sample rate)\n");
  }

  read_u32(in, &ByteRate); /* ignore */

  read_u16(in, &BlockAlign); /* ignore */

  read_u16(in, &BitsPerSample);
  if (BitsPerSample != BITS_PER_SAMPLE) {
    fatal_error("Bad wave header (Unexpected bits per sample)\n");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "data", 4u) != 0) {
    fatal_error("Bad wave header (no 'data' subchunk ID)\n");
  }

  /* finally we're at the Subchunk2Size field, from which we can
   * determine the number of samples */
  read_u32(in, &Subchunk2Size);
  *num_samples = Subchunk2Size / NUM_CHANNELS / (BITS_PER_SAMPLE/8u);
}

/*
 * Finds the air pressure, p, for each sample using the sin function. Converts this
 * value to fit within in16_t then stores it in the array buf.
 * Parameters:
 *   buf - array that stores the note at each sample
 *   num_samples - the number of stereo samples in the array
 *   channel - the channel to play the note (Left = 0, Right = 1)
 *   freq_hz - the frequency of the sin wave in Hz (cycles per second)
 *   amplitude - the amplitude of sin wave, ranges from 0 to 1 
 */
void render_sine_wave(int16_t buf[], unsigned num_samples, unsigned channel,
		      float freq_hz, float amplitude) {
  for (int i = 0; i < (int) num_samples; i++) {
    int index = channel + 2 * i;
    double t = ((double) i) / ((double) SAMPLES_PER_SECOND);

    //finds the value of the sin wave and multiplies it with the amplitude
    double temp  = amplitude * sin(t * freq_hz * 2.0 * PI);

    //scale temp to fit the data type int16_t
    buf[index] = clip(32767 * temp);
  }
}

/*
 * Calls render_sin_wave once for the right channel and once for the left
 * 0 = left, 1 = right.
 */
void render_sine_wave_stereo(int16_t buf[], unsigned num_samples,
			     float freq_hz, float amplitude) {
  render_sine_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_sine_wave(buf, num_samples, 1, freq_hz, amplitude);
}

/*
 * Finds the air pressure of a square wave function at each point in time t, computed 
 * from the current index of an int16_t array buf. Using a sine wave, the function sets 
 * the value in the array to be the maximum value if the sine wave is positve, and 
 * minimum value if the sine wave is negative.
 * Parameters:
 *   buf - array that stores the note at each sample
 *   num_samples - the number of stereo samples in the array
 *   channel - the channel to play the note (Left = 0, Right = 1)
 *   freq_hz - the frequency of the sin wave in Hz (cycles per second)
 *   amplitude - the amplitude of sin wave, ranges from 0 to 1 
 */
void render_square_wave(int16_t buf[], unsigned num_samples, unsigned channel,
			float freq_hz, float amplitude) {
  //calls render sine wave to initialize buf
  render_sine_wave(buf, num_samples, channel, freq_hz, amplitude);

  int prev_temp = -1; //makes it so first value at i = 0 is positive

  //loops through array, sets all positive values to max and negative
  //values to min. Values at 0 are set based on previous value
  for (int i = 0; i < (int) num_samples; i++) {
    int index = channel + 2 * i;
    int temp = (int) buf[index];
    if (temp == 0) { 
      if (prev_temp < 0) {
	temp = 1.0; //if last value was negative, set to positive 
      } else {
	temp = -1.0; //if last value was positive, set to negative
      }      
    }
    if (temp > 0) {
      buf[index] = clip(amplitude * 32767.0);
    }
    else {
      buf[index] = (amplitude * -32768.0);
    }
    prev_temp = temp;
  }
}

/*
 * Calls render_square_wave once for the right channel and once for the left
 * 0 = left, 1 = right.
 */
void render_square_wave_stereo(int16_t buf[], unsigned num_samples,
			       float freq_hz, float amplitude) {
  render_square_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_square_wave(buf, num_samples, 1, freq_hz, amplitude);
}
/*
 * Creates a sound wave that increases from the lowest possible value represented
 * by -amplitude to the highest possible value, amplitude, linearly. The function
 * stores the values in an array, making a saw wave.
 * Parameters:
 *   buf - array that stores the note at each sample
 *   num_samples - the number of stereo samples in the array
 *   channel - the channel to play the note (Left = 0, Right = 1)
 *   freq_hz - the frequency of the sin wave in Hz (cycles per second)
 *   amplitude - the amplitude of sin wave, ranges from 0 to 1 
 */
void render_saw_wave(int16_t buf[], unsigned num_samples, unsigned channel,
		     float freq_hz, float amplitude) {
  int num_cycles = 1; //trakcs current cycle, starting with the first
  double cycle_length = 1.0 / ((double) freq_hz); //length of one cycle
  double slope = (2.0 * amplitude) / cycle_length; //slope of sawtooth's line
  
  for (int i = 0; i < (int) num_samples; i++) {
    int index = ((int) channel) + 2 * i;
    double t  = ((double) i) / ((double) SAMPLES_PER_SECOND);

    
    if (t > (cycle_length * num_cycles)) {
      num_cycles++;
    }
    
    double temp = slope * t - amplitude * (2 * num_cycles - 1); 
    buf[index] = clip(temp * 32767);
  }
}
/*
 * Calls render_saw_wave once for the right channel and once for the left
 * 0 = left, 1 = right.
 */
void render_saw_wave_stereo(int16_t buf[], unsigned num_samples,
			    float freq_hz, float amplitude) {
  render_saw_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_saw_wave(buf, num_samples, 1, freq_hz, amplitude);
}
/*
 * Calls either render_sin, render_saw, or render_square based on
 * the passed value of voice. 0 calls sine_wave, 1 calls square_wave, 
 * and 2 calls saw_wave. All other values cause a fatal error.
 * Parameters:
 *   buf - array that stores the note at each sample
 *   num_samples - the number of stereo samples in the array
 *   channel - the channel to play the note (Left = 0, Right = 1)
 *   freq_hz - the frequency of the sin wave in Hz (cycles per second)
 *   amplitude - the amplitude of sin wave, ranges from 0 to 1
 *   voice - holds values 0, 1, or 2, corresponds to what function to call
 */
void render_voice(int16_t buf[], unsigned num_samples, unsigned channel,
		  float freq_hz, float amplitude, unsigned voice) {
  switch(voice) {
  case 0:
    render_sine_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  case 1:
    render_square_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  case 2:
    render_saw_wave(buf, num_samples, channel, freq_hz, amplitude);
    break;
  default:
    fatal_error("Bad value for voice. Must be 0, 1, or 2.\n");
    break;
  }
}
/*
 * Calls render_voice function once for the right channel and once for the left
 * 0 = left, 1 = right.
 */
void render_voice_stereo(int16_t buf[], unsigned num_samples, float freq_hz,
			 float amplitude, unsigned voice) {
  //blanks array, initializes it to 0
  blank_buf(buf, num_samples);

  render_voice(buf, num_samples, 0, freq_hz, amplitude, voice);
  render_voice(buf, num_samples, 1, freq_hz, amplitude, voice);
}
/*
 * Clips the passed double before it is converted to int16_t and placed
 * into the arrays of the render_wave functions. Ensures that wrap-around
 * does not occur.
 * Parameters:
 *    buf_val - the value to potentially clip
 */
int16_t clip(double buf_val) {
  if (buf_val > 32767) {
    return 32767;
  } else if (buf_val < -32768) {
    return -32768;
  } else {
    return (int16_t) buf_val;
  }
}
/*
 * Functions exactly the same as clip, capping values above or below the expected amount, but 
 * for integers intead of doubles
 * Parameters:
 *    buf_val - the value to possibly clip
 */
int16_t clip_int(int buf_val) {
  if (buf_val > 32767) {
    return 32767;
  } else if (buf_val < -32768) {
    return -32768;
  } else {
    return (int16_t) buf_val;
  }
}

/*
 * Initialized the array that will hold the pressure values for each
 * wave to 0.
 * Parameters:
 *   buf - array that stores the note at each sample
 *   num_samples - the number of stereo samples in the array
 */
void blank_buf(int16_t buf[], unsigned num_samples) {
  int stereo_samples = 2 * ((int) num_samples);
  for (int i = 0; i < stereo_samples; i++) {
    buf[i] = 0;
  }
}
