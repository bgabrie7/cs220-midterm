#include "wave.h"
#include "io.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>


int main(int argc, char * argv[]){
  //check for the correct number of arguments
  if (argc != 6) {
    fatal_error("Incorrect number of arguments.\n");
  }

  // converts voice, frequency, and amplitude from command line argument to unsigned int, float, and float respectively
  unsigned int voice = (unsigned int) atoi(argv[1]);
  float frequency = atof(argv[2]);
  float amplitude = atof(argv[3]);


  // check that amplitude is within the proper range
  if (amplitude < 0 || amplitude > 1) {
    fatal_error("Amplitude must be between 1 and 0.\n");
  }

  //converts stereo num_samples from commandline argument to unsigned int
  unsigned int num_samples = (unsigned int) atoi(argv[4]);
  if (num_samples <= 0) {
    fatal_error("Must be at least 1 stereo sample.\n");
  }


  //standard file opening and check for errors
  FILE *output = fopen(argv[5], "wb");
  if (ferror(output)) {
    fatal_error("Error opening file to write to.\n");
  }

  //malloc a buffer array in heap
  int16_t * buf =  (int16_t*) malloc(num_samples * 2 * sizeof(int16_t));
  if (buf == NULL) {
    fatal_error("malloc failed, buffer empty.\n");
  }

  //calls the render voice stereo function from wave.c, fills the buffer array with audio samples
  render_voice_stereo(buf, num_samples, frequency, amplitude, voice);

  //write wav file header and copy buffer array into wav file
  write_wave_header(output, num_samples);
  write_s16_buf(output, buf, num_samples * 2);

  //close files and free malloced memory
  free(buf);
  fclose(output);

  return 0;
}
