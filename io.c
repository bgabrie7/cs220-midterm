#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "io.h"

void fatal_error(const char* message) {
  printf(message);
  exit(1);
}

void write_byte(FILE* out, char val) {
  fputc(val, out);
}

void write_bytes(FILE* out, const char data[], unsigned n) {
  for (int i = 0; i < (int) n; i++) {
    fputc(data[i], out);
  }
}

void write_u16(FILE* out, uint16_t value) {
  fputc(value & 0xFF, out);
  fputc((value >> 8) & 0xFF, out);
}

void write_u32(FILE* out, uint32_t value) {
  fputc(value & 0xFF, out);
  fputc((value >> 8) & 0xFF, out);
  fputc((value >> 16) & 0xFF, out);
  fputc((value >> 24) & 0xFF, out);
}

void write_s16(FILE *out, int16_t value) {
  fputc(value & 0xFF, out);
  fputc(value >> 8 & 0xFF, out);
}

void write_s16_buf(FILE *out, const int16_t buf[], unsigned n) {
  for (int i = 0; i < (int) n; i++) {
    write_s16(out, buf[i]);
  }
}

void read_byte(FILE *in, char *val) {
  *val = fgetc(in);
}

void read_bytes(FILE *in, char data[], unsigned n) {
  for (int i = 0; i < (int) n; i++) {
    read_byte(in, &data[i]);
  }
}
void read_u16(FILE *in, uint16_t *val) {
  char byte1 = fgetc(in);
  char byte2 = fgetc(in);
  *val = byte1 | (byte2 << 8);
}

void read_u32(FILE *in, uint32_t *val) {
  uint8_t byte1 = fgetc(in);
  uint8_t byte2 = fgetc(in);
  uint8_t byte3 = fgetc(in);
  uint8_t byte4 = fgetc(in);
  *val = byte1
        | (byte2 << 8 & 0xFF00)
        | (byte3 << 16 & 0xFF0000)
        | (byte4 << 24 & 0xFF000000);
}
void read_s16(FILE *in, int16_t *val) {
  uint8_t byte1 = fgetc(in);
  uint8_t byte2 = fgetc(in);
  *val = byte1 | (byte2 << 8 & 0xFF00);
}

void read_s16_buf(FILE *in, int16_t buf[], unsigned n) {
  for (int i = 0; i < (int) n; i++) {
    read_s16(in, &buf[i]);
  }
}
