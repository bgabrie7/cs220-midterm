#include "wave.h"
#include "io.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>


//typing saver function that just calls a fatal error whenever the format is invalid
void invalid();
//function to convert an int midi value to the frequency value
double midiconversion(int);

int main(int argc, char * argv[]){

  // checks fro valid number of input arguments
  if (argc != 3) {
    fatal_error("Not enough arguments.\n");
  }
  //standard input and output
  FILE *input = fopen(argv[1], "r");
  FILE *output = fopen(argv[2], "wb");

  //checks for file open status
  if (ferror(output)) {
    fatal_error("Error opening file to write to.\n");
  }
  if (ferror(input)){
    fatal_error("Error opening song file.\n");
  }
  
  //checks the song txt's header, and stores the sample size as well as samples per beat in respective variables
  unsigned int num_samples;
  int samples_per_beat;
  if (fscanf(input, "%u %d ", &num_samples, &samples_per_beat) != 2){
    invalid();
  }
  
  //creates a heap buffer array. the variable buf is a pointer pointing to the first element of the array
  //size is num_samples *2, since rendering should be stereo
  int16_t * buf =  malloc(sizeof(int16_t) * (int) num_samples*2);
  //initalizes the array to be 0, silent
  blank_buf(buf, num_samples);


  //variable for the current amplitude, defaulted to 0.1. can be modified by directive 'A'
  float cur_amp = 0.1;
  //variable for current choice of voice, defaulted to sine wave. modified by directive 'V'
  unsigned int cur_voice = 0;

  //cursor pointer variable. it follows the directives and indicates the end of the previous directive and the location of the start of the new directive
  int16_t * cur = buf;

  //user input cirective
  char directive = ' ';

  //user input number of beats for a directive
  float beats = 0;

  //user input midi number
  int note = 0;

  //SIZE is the amount of samples that a directive occupies. (beats * samples per beat) This variable is nonstereo
  unsigned int SIZE = 0;


  //after the header, check for character indicating directive
  while(fscanf(input, "%c ", &directive) == 1){
    switch(directive){

    case 'A':
      //check for float value denoting ampltude after A
      //cursor does not move
      if (fscanf(input, "%f ", &cur_amp) != 1){
	invalid();
      } else if (cur_amp < 0.0 || cur_amp > 1.0){ //check whether the range of amplitude is valid
        fatal_error("Amplitude out of range\n");
      }
      break;

      
    case 'V':
      //check for unsigned int value denoting what voice to change to
      //cursor does not move
      if (fscanf(input, "%u ", &cur_voice) != 1){
	invalid();
      }
      break;

      
    case 'P':
      //check for beats value to pause
      if (fscanf(input, "%f ", &beats) != 1){
	invalid();
      }
      //moves the cursor by an index of SIZE*2, to account for stereo
      SIZE = (unsigned int)(beats * samples_per_beat);
      cur += SIZE * 2;
      break;

      
    case 'N':
      //check for beats and midi note number, assigns them to respective variable
      if (fscanf(input, " %f %d ", &beats, &note) != 2){
	invalid();
      }
      SIZE = (unsigned int)(beats * samples_per_beat);
      //render a voice based on current amp, voice, size and teh return result of the function midiconversion. Inserted at the location of cursor
      render_voice_stereo(cur, SIZE, midiconversion(note), cur_amp, cur_voice);
      //move cursor after voice is rendered
      cur += SIZE * 2;
      break;

      
    case 'C':
      //chcek for beats
      if (fscanf(input, " %f ", &beats) != 1){
	invalid();
      }
      SIZE = (unsigned int) (beats * samples_per_beat);

      //mallocs a temp array to which new notes of the chord are stored to before adding to the buffer array
      int16_t * temp = malloc(sizeof(int16_t) * SIZE * 2);

      //check for midi note number, until the number 999 shows
      while ((fscanf(input, " %d ", &note) == 1) && (note != 999)){
	
	//renders a voice to the temp array, based on current amp, voice, and etc.
	render_voice_stereo(temp, SIZE, midiconversion(note), cur_amp, cur_voice);

	//for loop adds the temp array to the location of cur
        for (int i = 0; i < (int)(SIZE * 2); i++){
	  cur[i] = clip_int(temp[i] + cur[i]);
	}
      }
      
      //move cur by SIZE*2
      cur += SIZE * 2;
      //free the temporarily allocated array
      free(temp);
      break;
      
    default:
      //any other directive input
      fatal_error("Invalid directive\n");
    }
  }
  
  //write buffer array into output
  write_wave_header(output, num_samples);
  write_s16_buf(output, buf, num_samples*2);

  free(buf);
  fclose(output);
  fclose(input);

  return 0;
  
}


//additional functions
void invalid(){
  fatal_error("Song file in invalid format.\n");
}

double midiconversion(int midi){
  double midi_val = (double) midi;
  return (440* pow(2.0, (midi_val - 69.0)/12.0));
}
