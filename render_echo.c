#include "wave.h"
#include "io.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char * argv[]){

  //verifying that there is a correct amount of arguments
  if (argc != 5) {
    fatal_error("Incorrect number of arguments.\n");
  }

  //standard input and output stuff.
  FILE *output = fopen(argv[2], "wb");
  FILE *input = fopen(argv[1], "rb");

  //delay is the number of stereo samples that the echoed audio is transposed by, assuming that the project only requires us to add one echoed sample
  int delay = atoi(argv[3]);
  //ampltude is the relative amplitude of the echo effect
  float amplitude = atof(argv[4]);

  // the length of the original audio file. In the form of a pointer because the actual value is only assigned from the read_wave_header function
  unsigned num_samples = 0;

  //checks if files are opened correctly
  if (output == NULL || ferror(output)) {
    fatal_error("Error opening file to write to.\n");
  }
  if (input == NULL || ferror(input)) {
    fatal_error("Error opening file to read from.\n");
  }

  //reads wave header, obtain num_samples
  //num_samples obtained in this function should be total (stereo) total sample size
  read_wave_header(input, &num_samples);
  

  //standard malloc for buffer
  int16_t * buf = (int16_t*) calloc(num_samples * 2, sizeof(int16_t));

  if (buf == NULL){
    fatal_error("Calloc failed");
  }
  //reads values from input file
  read_s16_buf(input, buf, num_samples * 2);
  
  //adds a shifted and damped version of the original audio to the array, shifted by an index of (delay*2), since delay is stereo
  int16_t * temp = (int16_t*) calloc(num_samples * 2, sizeof(int16_t));
  if (temp == NULL){
    fatal_error("calloc failed for temp");
  }

  for (int i = 0; i < 2 * (int)(num_samples - delay); i++) {
    temp[i + delay * 2] = buf[i] * amplitude;
  }

  for(int i = 0; i < 2 * (int) num_samples; i++) {
    buf[i] = clip_int(buf[i] + temp[i]);
  }
  
  //write wave file and header, same format as other render programs
  write_wave_header(output, num_samples);
  write_s16_buf(output, buf, num_samples * 2);

  free(buf);
  free(temp);
  fclose(output);
  fclose(input);
  
  return 0;
}
